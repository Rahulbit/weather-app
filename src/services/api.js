import fetchJsonp from 'fetch-jsonp';


const getCityForcast = (city = 'Pune') => fetchJsonp('https://api.openweathermap.org/data/2.5/forecast?' + `q=${city}&APPID=e7ebdca5be2bbb4c3b1230a8cd7a8df1`);



export default {
  getCityForcast,
}

