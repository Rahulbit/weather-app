import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './App.css';


import AsyncHome from './components/Home';
import AsyncAbout from './components/About';
import Search from './components/Search';
import Alert from './components/Alert';

class App extends Component {
    constructor(){
      super();
    }

    render(){
      return (
        <Router>
        <div className="full-height">
          <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container-fluid">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <Link to="/" className="navbar-brand">Weather Forcast</Link>
              </div>
              <div id="navbar" className="navbar-collapse collapse">
                <ul className="nav navbar-nav navbar-right">
                  <li><Link to="/">Home</Link></li>
                  <li><Link to="/about">About</Link></li>
                </ul>
                <Search/>
              </div>
            </div>
          </nav>

          <div className="container-fluid full-height">
            <div className="row full-height">
              <div className="col-xs-12 main full-height">
                  <Route exact path="/" component={AsyncHome} />
                  <Route path="/About" component={AsyncAbout} />
              </div>              
            </div>
          </div>
          <Alert />
        </div>
      </Router>
      )
    }
}

export default App;