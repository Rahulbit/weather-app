import { combineReducers } from 'redux';
import weather from './components/Home/reducer';
import alert from './components/Alert/reducer';

const nav = function(state={}, action){
	return {...state};
}

const rootReducer = combineReducers({
  nav,
  weather,
  alert
});

export default rootReducer;
