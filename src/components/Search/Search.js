import React, { PureComponent } from 'react';
import * as homeActions from './../Home/actions';
import { connect }  from 'react-redux';
import { bindActionCreators } from 'redux';

class Search extends PureComponent{
	constructor(){
		super();
		this.state = {
			city : 'Pune',
		}
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.getCityForcast(this.state.city);
	}

	render(){
		return (
        <form className="navbar-form navbar-right" role="search" onSubmit={this.handleSubmit}>
	        <div className="input-group">
	            <input 
	            type="text" 
	            className="form-control" 
	            placeholder="City" 
	            onChange={(e) => this.setState({city : e.target.value})}
	            value={this.state.city} 
	            />
	            <div className="input-group-btn">
	                <button className="btn btn-default" type="submit"><i className="glyphicon glyphicon-search"></i></button>
	            </div>
	        </div>
        </form>
		);
	}
}

const mapStateToProps = state => {return {search: state.search}};
const mapDispatchToProps = dispatch => bindActionCreators({...homeActions}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Search);
//export default Search;
