import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { closeAlert } from './actions';
import classnames from 'classnames';
import { Modal } from 'react-bootstrap';

class Alert extends PureComponent {
  constructor(props) {
    super(props);
  }

  _handleOk = (e) => {
    if(this.props.onConfirm && typeof this.props.onConfirm == 'function'){
      this.props.onConfirm(this.props.params);
    }
    this.props.closeAlert();
  }

  render() {
    let headingClass = this.props.type == 'error' ? 'danger' : this.props.type;
    let type = this.props.type || '';
    let heading = type.charAt(0).toUpperCase() + type.slice(1);
    return (
      <Modal show={this.props.isVisible} className="alert-modal">
        <Modal.Body>
          <div className={`panel panel-${headingClass}`}>
            <div className="panel-heading">{heading}</div>
            <div className="panel-body">
              <p>
                {this.props.message}
              </p>  
              <button onClick={this._handleOk} className="btn btn-primary col-sm-4 col-sm-offset-4">Ok</button>
            </div>
          </div>
        </Modal.Body>
      </Modal>

    );


  }
}

function mapStateToProps(state) {
  return {
    ...state.alert
  }
}

export default connect(mapStateToProps, { closeAlert })(Alert);

//export default Alert;

