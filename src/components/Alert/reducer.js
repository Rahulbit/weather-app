import { SHOW_ALERT, CLOSE_ALERT } from './actions';

export default function alert(state = { isVisible: false, message: '', }, action) {
  switch (action.type) {
    case 'SHOW_ALERT':
        return { ...state, 
                  type: action.payload.type,
                  isVisible: true, 
                  message: action.payload.message, 
                }
      break;
    case 'CLOSE_ALERT':
        return { ...state, isVisible: false, message: '', type: 'info', }
      break;
    default:
      return state
  }
}
