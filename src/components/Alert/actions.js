
export const showAlert = payload => {
  return {
    type: 'SHOW_ALERT',
    payload: payload
  }
}

export const closeAlert = payload => {
  return {
    type: 'CLOSE_ALERT',
    payload: false
  }
}
