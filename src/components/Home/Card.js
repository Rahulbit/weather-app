import React, { PureComponent } from 'react';
import moment from 'moment';

class Card extends PureComponent{
	constructor(){
		super()
	}


	render(){
      
      let { date, data } = this.props;
		return (

      <div className="panel panel-default">
         <div className="panel-heading">{moment(date).format("dddd, MMMM Do YYYY")}</div>
            <table className="table table-condensed table-bordered">
               <thead>
                  <tr> 
                     <th>Time</th>
                     <th>Weather condition</th>
                     <th>Cloudiness %</th>
                     <th>Temperature</th>
                     <th>Pressure</th>
                     <th>Humidity %</th>
                     <th>Wind</th>                     
                  </tr> 
               </thead> 
               <tbody>
                  {data.map((item, index) => {
                     let { dt, clouds, main, sys, weather, wind } = item;
                     return (
                        <tr key={index}>
                           <th scope="row">{moment.unix(dt).format("hh:mm A")}</th>
                           <td>{weather[0].description} <img src={`https://openweathermap.org/img/w/${weather[0].icon}.png`}/></td>
                           <td>{clouds.all} %</td>
                           <td>{main.temp}</td>
                           <td>{main.pressure}</td>
                           <td>{main.humidity} %</td>
                           
                           <td>{wind.speed}, {wind.deg}</td>
                        </tr>
                     );
                  })}
                  
               </tbody>
            </table>
      </div>
		);
	}
}


export default Card;
