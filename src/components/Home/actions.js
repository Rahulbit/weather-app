import api from './../../services/api';

export const getCityForcast = city => {
	return dispatch => {
		api
		.getCityForcast(city)
		.then(result => result.json())
		.then(result => {
			return dispatch({
				type: 'SET_WEATHER',
				payload: result
			})
		})
		.catch(err => {
			return dispatch({
				type: 'SHOW_ALERT',
				payload : {
					type: 'error',
					message: 'Opps! something went wrong! Please Try Again!'
				}
			})
			console.log('__err__', err);
		})
	}
}