import React, { PureComponent } from 'react';
import * as homeActions from './actions';
import { connect }  from 'react-redux';
import { bindActionCreators } from 'redux';
import Loader from './../Loader';

import Cards from './Cards';
class Home extends PureComponent{
	constructor(){
		super()
	}

	componentDidMount(){
		this.props.getCityForcast();
	}

	render(){
		const  { weather } = this.props;
		if(!weather.list){
			return <Loader />;
		}
		return (
			<div className="main-wrapper">
				<h1>{weather.city.name} Forcast</h1>
         		<Cards cards={weather.list}/>
      		</div>
		);
	}
}

const mapStateToProps = state => {return {weather: state.weather}};
const mapDispatchToProps = dispatch => bindActionCreators({...homeActions}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
//export default Home;
