import React, { PureComponent } from 'react';
import Card from './Card';
import moment from 'moment';





class Cards extends PureComponent{

	constructor(){
		super()
	}

   renderCards = () => {
      
      let list = this.props.cards.reduce((list, item) => {
         let date = moment.unix(item.dt).local().format('Y-M-D');
         if(!list[date]){
            list[date] = [];
         }
         list[date].push(item);
         return list;
      }, []);

      return Object.keys(list).map(key => {
         return <Card key={key} data={list[key]} date={key}/>
         
         //return list[key];
      })
   }

	render(){

      //const data = this.transfromCards();

		return (
         <div>
            {
               this.renderCards()
            }
         </div>
		);
	}
}


export default Cards;
