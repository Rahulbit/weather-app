/* @flow */
import { compose, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducer';
import { createLogger as logger } from 'redux-logger';


export default function configureStore(initialState) {

  initialState = {
    ...initialState
  }


  let createStoreWithMiddleware;
  
  //if (__DEVELOPMENT__) {
  if (process.env.NODE_ENV !== 'production' && process.env.IS_BROWSER) {
    createStoreWithMiddleware = compose(
      applyMiddleware(thunk, logger()), // any Redux middleware, e.g. redux-thunk
      //DevTools.instrument(),
      //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )(createStore);

    if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept('./reducer', () => {
        const nextReducer = require('./reducer').default;
        store.replaceReducer(nextReducer);
      });
    }
  } else {
    createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
  }

  //createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

  //console.log('___initial__state', initialState);
  const store = createStoreWithMiddleware(reducer, initialState);
  //console.log(store);
  return store;
}
