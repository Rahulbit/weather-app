import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './App';

import { Provider } from 'react-redux';
import createStore from './store';

const renderApp = () => {
  render(
  	<Provider store={createStore()}>
    	<AppContainer>
      		<App />
    	</AppContainer>
    </Provider>,
    document.getElementById('root'),
  );
};

// This is needed for Hot Module Replacement
if (module.hot) {
  module.hot.accept('./App', () => renderApp());
}

renderApp();
