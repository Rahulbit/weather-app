# Weather App

## Demo

[https://build-cpkazymunp.now.sh](https://build-cpkazymunp.now.sh)

## Requirements

- Node.js v6.9+
- Yarn or npm client

## Installation

```bash
❯ cd weather-app/
❯ yarn
```

## Quick start

### Yarn

```bash 
❯ yarn start
✔ Development server running on: http://localhost:5000
✔ Build completed
```

### npm
````bash
❯ npm start
✔ Development server running on: http://localhost:5000
✔ Build completed
````

## Building

`weather-app` builds static assets to the `build` directory by default when running `yarn build`.

````bash
❯ yarn build
✔ Building project completed
````


##Todo
 - Add autocomple serch for cities
 - provide weather metric options (celsius/fahrenheit)
 - store searched cities in localstrorage
